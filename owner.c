#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"


void owner_area(user_t *u)
{
    int choice;
    do{
            printf("Enter your choice \n:");
            printf("\n\n0.Sign out\n1.Appoint Librarian\n2.Edit Profile\n3.Change password\n4.Fees and File report\n5.Book Availability\n6.Book Catgories or subjects\n");
            scanf("%d",&choice);
            switch(choice)
            {
                case 1: //this case is appoint the libririan.
                        appoint_libarian();
                        break;
                case 2:// edit profile 
                       edit_profile_by_id();
                       break; 
                case 3://change password.
                       change_pwd_by_email();
                        break;
                case 4:
                   break; 
                case 5:
                   break; 
                case 6:
                    break; 
             }       
     }  
      while(choice!=0);
}

void appoint_libarian()
{
   // take input librarian details.
      user_t u;
      user_accept(&u);
     
   // change user role to librarian.
     strcpy(u.role, ROLE_LIBRARIAN);
   // adding librarian into the users file.
      add_user(&u);
}
