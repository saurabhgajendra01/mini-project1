#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
#include "date.h"

void date_tester() {
	date_t d1 = {1, 1, 2000}, d2 = {31, 12, 2000};
	date_t d = {1, 1, 2000};
	date_t r = date_add(d, 366);
	date_print(&r);
	int diff = date_cmp(d1, d2);
	printf("date diff: %d\n", diff);
}

void testing() 
{
	user_t u;
	book_t b;
	user_accept(&u);
	user_display(&u);
	book_accept(&b);
	book_display(&b);
}

void sign_in()
{
			char email[40], password[15];
				user_t u;
				int invalid_user = 0;
				// input email and password from user.
					printf("enter email: ");
					scanf("%s", email);
					printf("enter password: ");
					scanf("%s", password);
				// find the user in the users file by email.
				if(find_user_by_email(&u, email) == 1) 
				  {
					// checking input password is correct or not.
					if(strcmp(password, u.pwd) == 0) 
					 {
						// special case: check for owner login 
						if(strcmp(email, EMAIL_OWNER) == 0)
							strcpy(u.role, ROLE_OWNER);

						// if correct data, call user_area() based on its role (or its respected areas).
						if(strcmp(u.role, ROLE_OWNER) == 0)
							  owner_area(&u);
						else if(strcmp(u.role, ROLE_LIBRARIAN) == 0)
							librarian_area(&u);
						else if(strcmp(u.role, ROLE_MEMBER) == 0)
							member_area(&u);
						else
							invalid_user = 1;
					}
					else
						invalid_user = 1;
				}
				else
					invalid_user = 1;

				if(invalid_user)
					printf("Entered Invalid email, password or role.\n");
}
void sign_up()
{
	// take input user detail from user.
      user_t u;
	  user_accept(&u);
	// Stoare the user detail into users file.
      add_user(&u);
}

int main()
{
    printf("hello world..!\n");
    //testing();
	//int id=get_max_id();
	//printf("the last id :%d\n",id);

	int choice;
	do
	{
			printf("please enter your choice:");
			printf("\n0.Exit:\n1.Sign In:\n2.Sign Up:");
			scanf("%d",&choice);
			switch(choice)
			{
				case 1:  // Sign_in Function
				          sign_in();
					break;
				case 2:  //  Sign_up Function 
				        sign_up();
					break;		
			}
	}
	while(choice!=0);
return 0;
}