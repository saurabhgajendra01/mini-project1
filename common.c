#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "library.h"

// users function definaatins
void user_accept(user_t *u)
{
   // printf("enter id:");
    //scanf("%d",&u->id);
    u->id=get_max_user_id();
    printf("enter your name:");
    scanf("%s",u->name);
    printf("enter your email:");
    scanf("%s",u->email);
    printf("enter your phoneno:");
    scanf("%s",u->phone);
    printf("enter your password:");
    scanf("%s",u->pwd);
   // printf("enter your role:");
   // sacnf("%s",u->role);
   strcpy(u->role,ROLE_MEMBER);
}
//display user info.
void user_display(user_t *u)
{
    printf("id:%d ,name:%s ,email:%s,phone:%s ,role:%s\n",&u->id, u->name, u->email, u->phone, u->role);
}

//book functions definations
 void book_accept(book_t *b)
 {
    //printf("enter book id:");
    //scanf("%d",&b->id);

    printf("enter book name:");
    scanf("%s",b->name);
    printf("enter book auther name:");
    scanf("%s",b->author);
    printf("book subject:");
    scanf("%s",b->subject);
    printf("enter book price:");
    scanf("%lf",&b->price);
    printf("isbn code:");
    scanf("%s",b->isbn);
 }
   void book_display(book_t *b)
    {
        printf("book id:%d,book name:%s,author name:%s, book subject:%s, price: %.2lf, isbn:%s\n",b->id,b->name,b->author,b->subject,b->price,b->isbn);
    }


// Add user function defination.
void add_user(user_t *u)
{
    FILE *fp;
  	fp = fopen(USER_DB, "ab"); //open users file.
	  if(fp == NULL) 
    {
		perror("failed to open users file");// print error to use
		return;
  	}

	// stoare user data into the file
    	fwrite(u, sizeof(user_t), 1, fp);
	    printf("user added into file.\n");
	
	// close the file
	 fclose(fp);
}

// find user function defination.
int find_user_by_email(user_t *u,char email[])
{
         FILE *fp;
        int found = 0;
        // open the file for reading the  users data
        fp = fopen(USER_DB, "rb");
         if(fp == NULL) 
         {
          perror("failed to open users file"); // printing the error if file is not present
          return found;
        }
        // read all users data one by one
        while(fread(u, sizeof(user_t), 1, fp) > 0) 
        {
          // if user email is matching, found 1
          if(strcmp(u->email, email) == 0) 
          {
            found = 1;
            break;
          }
        }
        // close file
        fclose(fp);
        return found;

}

// Bookcopies functions defination
void bookcopy_accept(bookcopy_t *bc)
{
   printf("Enter the id:");
   scanf("%d",&bc->id);
   printf("Enter the book id:");
   scanf("%d",&bc->bookid);
   printf("Enter rack:");
   scanf("%d",&bc->rack);
   strcpy(bc->status, STATUS_AVAIL);
}

 void bookcopy_display(bookcopy_t *bc)
 {
    printf("The id:%d, The Bookid:%d, The rack:%d, The status:%s");
 }

// issurecord function definations
void issuerecord_accept(issuerecord_t *ir)
{
    //printf("Enter issue id: ");
    //scanf("%d", &ir->id);

    printf("Enter copy id: ");
    scanf("%d", &ir->copyid);
    printf("Enter member id: ");
    scanf("%d", &ir->memberid);
    printf("The issue :");
    date_accept(&ir->issue_date);

    //r->issue_date = date_current();

    ir->return_duedate = date_add(ir->issue_date, BOOK_RETURN_DAYS);
    memset(&ir->return_date, 0, sizeof(date_t));
    ir->fine_amount = 0.0; 
}

void issuerecord_display(issuerecord_t *ir)
{
     printf("issue record Id: %d,Book Copy Id: %d, the member Id: %d, find: %.2lf\n", ir->id, ir->copyid, ir->memberid, ir->fine_amount);
     printf("issue Date:");
	   date_print(&ir->issue_date);
	   printf("return due date: ");
	   date_print(&ir->return_duedate);
     printf("return date: ");
	   date_print(&ir->return_date);
}

// payment finctions definations
void payment_accept(payment_t *p)
{
   printf("enter payment id:");
   scanf("%d",&p->id);
   printf("Enter member id:");
   scanf("%d",&p->memberid);
   printf("enter Type of payment (fees/fine");
   scanf("%s",p->type);
   printf("enter payment amount");
   scanf("%d", &p->amount);

    p->tx_time = date_current();
	  if(strcmp(p->type, PAY_TYPE_FEES) == 0)
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	 else
	  	memset(&p->next_pay_duedate, 0, sizeof(date_t));
}

void payment_display(payment_t *p)
{
    printf("payment id: %d, member id: %d, type of payment:%s, the payment amount amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
	  printf("payment date:");
	  date_print(&p->tx_time);
  	printf("payment due date:");
	  date_print(&p->next_pay_duedate);

}

int get_max_user_id()
{
    FILE *fp;
    int max=0;
    long size=sizeof(user_t);
    user_t u;
   // first open the file
   fp=fopen(USER_DB, "rb");
  if(fp==NULL)
    return max+1; 
  // and change the position of cursor to last racord
    fseek(fp,-size,SEEK_END);
  // read the file record
  if(fread(&u, size ,1, fp)>0)
  {
      // if read file the get max(its own) id
    max=u.id;
  }
  //colse the file
  fclose(fp);
  //return max+1
  return max+1; 
}

//get new book id 
int get_max_book_id()
{
   FILE *fp;
    int max=0;
    long size=sizeof(book_t);
    book_t b;
   // first open the file
   fp=fopen(BOOK_DB, "rb");
  if(fp==NULL)
    return max+1; 
  // and change the position of cursor to last racord
    fseek(fp,-size,SEEK_END);
  // read the file record
  if(fread(&b, size ,1, fp)>0)
  {
      // if read file the get max(its own) id
    max=b.id;
  }
  //colse the file
  fclose(fp);
  //return max+1
  return max+1; 
}

//find the book by its name

void book_find_by_name(char name[])
{
          FILE *fp;
        int found = 0;
        book_t b;
        // open the file for reading the  users data
        fp = fopen(BOOK_DB, "rb");
         if(fp == NULL) 
         {
          perror("failed to open books file"); // printing the error if file is not present
          return;
        }
        // read all users data one by one
        while(fread(&b, sizeof(book_t), 1, fp) > 0) 
        {
          // if user book name  is matching partialy, found 1
          if(strstr(b.name, name)!= NULL) 
          {
            found = 1;
            book_display(&b);
          }
        }
        // close file
        fclose(fp);
        if(!found)
          printf(" NO such a book find in libaray");
}

// get max id to issuerecord.
int get_max_issuerecord_id()
{
      FILE *fp;
      int max=0;
      int size=sizeof(issuerecord_t);
      issuerecord_t ir;
      // open the issurecord file.
      fp=fopen(ISSUERECORD_DB,"rb");
      if(fp==NULL)
      {
        return max+1;
      }
      // change file position to the last record.
      fseek(fp, -size,SEEK_END);
      // read the file one by one.
      if (fread(&ir,size, 1,fp)>0)
      {
        // if file is read successful,get max id.
        max=ir.id;
      }
      // close the file.
      fclose(fp);
      // return the max id;
        return max+1;
}
// edit profile by users id.
void edit_profile_by_id()
{
        int id, found = 0;
        FILE *fp;
        user_t u;
        // input user id from user.
        printf("enter user id to be edit user profile : ");
        scanf("%d", &id);

        // open users file
        fp = fopen(USER_DB, "rb+");
        if(fp == NULL) 
        {
          perror("cannot open users file");
          exit(1);
        }
        // read users one by one and check if user with given id is found.
        while(fread(&u, sizeof(user_t), 1, fp) > 0) 
        {
          if(id == u.id) 
            {
            found = 1;
            break;
          }
        }
        // if found
        if(found) 
        {
          // input new book details from user
          long size = sizeof(user_t);
          user_t nu;
          user_accept(&nu);
          nu.id = u.id;

          // take file position one record behind.
          fseek(fp, -size, SEEK_CUR);

          // overwrite users details into the file
          fwrite(&nu, size, 1, fp);
          printf("user profile is editated.\n");
        }
        else // if not found
          // show message to user is not found.
          printf("user is not found.\n");
        // close user file
         fclose(fp);
}

// change pwd by its email.
void change_pwd_by_email()
{
    char email[20],oldpwd[10],newpwd[10];
    user_t u;
    FILE *fp;
    int found=0;
    // take input email from user to find user to change its pwd.
      printf("enter your email:");
      scanf("%s",email);
     // take user old pwd to over write it.
       printf("enter your old password:");
       scanf("%s",oldpwd);
       // open the file.
       fp=fopen(USER_DB,"rb+");
       if (fp==NULL)
       {
         perror("the user  file cannot open");
         return;
       }
       // find user to its email into in record.
       while(fread(&u, sizeof(user_t), 1, fp)>0)
       {
         if(strcmp(u.email,email)==0 && strcmp(u.pwd,oldpwd)==0)
         {
           found=1;
           break;
         }
       }
       
 // if found
  if(found)
  {
     long size=sizeof(user_t);
   // take input new password from user.
      printf("Enter the new paasword :");
      scanf("%s",u.pwd);
     // take file position one record behind.
      fseek(fp, -size, SEEK_CUR);
      //overwrite the password.
      fwrite(&u, size, 1, fp);

      printf("password is change successfully.");
  }
  else 
  {
    // not found 
    // show message to user user is not found.
    printf("user is not found.");
  }
   //close the file.
  fclose(fp);
}
