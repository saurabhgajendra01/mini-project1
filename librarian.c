#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"



void librarian_area(user_t *u)
{
   int choice;
   char name[85];
    do
      {
            printf("please Enter your choice \n:");
            printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\n");
            scanf("%d",&choice);
            switch(choice)
            {
                case 1: //add new member
                      add_new_member();
                       break;
                case 2: // edit profile
                       edit_profile_by_id();
                       break; 
                case 3:// change password its own.
                       change_pwd_by_email();
                       break;
                case 4:  // adding a new book into liabrary.
                        add_book();
                        break; 
                case 5: // find a perticular in library
                       printf("Enter book name:");
                       scanf("%s", name);
                       book_find_by_name(name);
                       break; 
                case 6: // edit book
                       book_edit_by_id();
                       break; 
                case 7: // check bookcopies available status.
                         bookcopy_checkavailable_details();    
  
                       break;
                case 8:
                   break; 
                case 9:
                    break;
                case 10: // bookcopy issue.
                          bookcopy_issue();
                      break; 
                case 11:
                   break; 
                case 12:
                    break; 
                case 13:
                    break;          
            }  
    }
      while(choice!=0);
}
void add_new_member()
{
   // take input members details.
      user_t u;
      user_accept(&u);
   // adding librarian into the users file.
      add_user(&u);
}     
// adding new book
void add_book()
{
   FILE *fp;
   // take input book details.
      book_t b;
      book_accept(&b);
      b.id=get_max_book_id();
   // adding books into the book file.
   // open books file
      fp=fopen(BOOK_DB, "ab");
       if(fp==NULL)
       {
          perror("cannot open the file ");
          exit(1);
       }

     //append book to the file
     fwrite(&b,sizeof(book_t),1,fp);

     // appends bookcopy to the file.
     fwrite(&b,sizeof(bookcopy_t), 1, fp);
     printf("book is added into the file ");
     // file is close
     fclose(fp);
}

// edit book record by its id
void book_edit_by_id()
{
   int id, found = 0;
	FILE *fp;
	book_t b;
	// input book id from user.
	printf("enter book id to be edit book : ");
	scanf("%d", &id);
	// open books file
	fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) 
   {
		perror("cannot open books file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&b, sizeof(book_t), 1, fp) > 0) 
   {
		if(id == b.id) 
      {
			found = 1;
			break;
		}
	}
	// if found
	if(found) 
   {
		// input new book details from user
		long size = sizeof(book_t);
		book_t nb;
		book_accept(&nb);
		nb.id = b.id;
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file
		fwrite(&nb, size, 1, fp);
		printf("book is updated.\n");
	}
	else // if not found
		// show message to user that book not found.
		printf("Book not found.\n");
	// close books file
	fclose(fp);
}

// check bookcopy available details.
void bookcopy_checkavailable_details()
{
   int book_id;
   FILE *fp;
   bookcopy_t bc;
   int count=0;

   // take input book id;
   printf("Enter the book id:\n");
   scanf("%d",&book_id);
   
    // open the bookcopy file.
    fp=fopen(BOOKCOPY_DB, "rb");
    if(fp==NULL)
    {
       perror("bookcopy file cannot open");
       return;
    }
    // read  bookcopy records are one by one .
    while (fread(&bc, sizeof(bookcopy_t), 1, fp)>0)
    {
        // if bookcopy id is match and status of bookcopy is available then print the bookcopy deatils.
       if(bc.bookid==book_id && strcmp(bc.status, STATUS_AVAIL)==0)
       {
          bookcopy_display(&bc);
          count++;
       }
    }
   // closing the file book copies file.
   fclose(fp);
   // if(counrt==0)
   {
      printf("no copies available in the bookcopies file");
   }      
}

// bookcopy issue function.
void bookcopy_issue()
{
   issuerecord_t ir;
   FILE *fp;
   // take issurecord details from the user.
   issuerecord_accept(&ir);
   // TODO: if user is not paid ,given error & return.
   // generate & assign new id for the issurecord.
   ir.id=get_max_issuerecord_id();
   // open the issuerecord file
   fp=fopen(ISSUERECORD_DB,"rb");
   if(fp==NULL)
   {
      perror("The issurecord file is cannot open");
      return;
   }
   // append record into the file.
   fwrite(&ir ,sizeof(issuerecord_t),1,fp);
   // close the file.
   fclose(fp);

   // bookcopy_change_status as a copy is issued.
     bookcopy_changestatus(ir.id,STATUS_ISSUED);
}

// change the bookcopy status.
void bookcopy_changestatus(int bookcopy_id, char status[])
{
         bookcopy_t bc;
         FILE *fp;
         long size= sizeof(bookcopy_t);
         //open the book copies file.
         fp=fopen(BOOKCOPY_DB,"rb+");
         if(fp==NULL)
         {
            perror("cannot open the book copies file");
            return;
         }
         // read one by one copies from bookcopies file.
         while(fread(&bc,sizeof(bookcopy_t), 1,fp)>0)
         {
            //if bookcopy id is matching.
            if(bookcopy_id==bc.id)
            {
               //moify the status.
               strcpy(bc.status, status);
               // go one record back.
               fseek(fp, -size, SEEK_CUR);
               // overwrite the record into the file.

            fwrite(&bc,sizeof(bookcopy_t) ,1 ,fp);
            break;
            }
         }
         // close the file.
         fclose(fp);  
      }

      // display issued bookcopies.
void display_issued_bookcopies(int member_id)
  {
         FILE *fp;
         issuerecord_t rec;
      // open issue records file

      fp = fopen(ISSUERECORD_DB, "rb");
      if(fp==NULL)
         {
         perror("cannot open issue record file");
         return;
         }

      // read records one by one
      while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) 
      {
         // if member_id is matching and return date is 0, print it.
         if(rec.memberid == member_id && rec.return_date.day == 0)
            issuerecord_display(&rec);
      }
      // close the file
      fclose(fp);
 }

// return issued bookcopy function 
  void bookcopy_return()
   {
         int member_id, record_id;
         FILE *fp;
         issuerecord_t rec;
         int diff_days, found = 0;
         long size = sizeof(issuerecord_t);
         // input member id
         printf("enter member id: ");
         scanf("%d", &member_id);
         // print all issued books (not returned yet)
         display_issued_bookcopies(member_id);
         // input issue record id to be returned.
         printf("enter issue record id (to return): ");
         scanf("%d", &record_id);
         // open issuerecord file
         fp = fopen(ISSUERECORD_DB, "rb+");
         if(fp==NULL) 
         {
            perror("cannot open issue record file");
            return;
         }
         // read records one by one
         while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) 
         {
            // find issuerecord id
            if(record_id == rec.id) 
            {
               found = 1;
               // initialize return date
               rec.return_date = date_current();
               // check for the fine amount
               diff_days = date_cmp(rec.return_date, rec.return_duedate);
               // update fine amount if any
               if(diff_days > 0)
                  rec.fine_amount = diff_days * FINE_PER_DAY;
               break;
            }
         }
         
         if(found) 
         {
            // go one record back
            fseek(fp, -size, SEEK_CUR);
            // overwrite the issue record
            fwrite(&rec, sizeof(issuerecord_t), 1, fp);
            // print updated issue record.
            printf("issue record updated after returning book:\n");
            issuerecord_display(&rec);
            // update copy status to available 
            bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
         }
         
         // close the file.
         fclose(fp);
}
