#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"


void member_area(user_t *u)
{
    int choice;
    char name[85];
    do
     {
            printf("\nEnter your choice:");
            printf("\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Check Book Availability\n");
            scanf("%d", &choice);
            switch(choice)
            {
                case 1: // find a book
                       printf("Enter book name:");
                       scanf("%s", name);
                       book_find_by_name(name);                    
                      break;
                case 2:// edit profile
                       edit_profile_by_id();
                    break; 
                case 3:// change password.
                        change_pwd_by_email();
                        break;
                case 4:  // check book avilability.
                        bookcopy_check_available();
                      break;  
             }      
        }  
      while(choice!=0);
 }     

 //  member to check book copy is available. 
  void bookcopy_check_available()
 {
     int book_id;
     FILE *fp;
     bookcopy_t bc;
     int count=0;

     // take book id is input.
     printf("Enter the book id:");
     scanf("%d", &book_id);

     // open the bookcopies flie.
     fp=fopen(BOOKCOPY_DB,"rb");
     if (fp==NULL)
     {
         perror("file cannot open");
         return;
     }
     
     //read bookcopy record file record by record.
     while(fread(&bc,sizeof(bookcopy_t), 1,fp)>0)
     {
         //if book id is matching and status of bookavailble is match then count the bookcpoy.
         if(bc.bookid==book_id && strcmp(bc.status,STATUS_AVAIL)==0)
         {
             count++;
         }
     }
     
     // close the bookfile.
     fclose(fp);
     // print the no of copies .
     printf("The number of copies is available:",count);
 } 
 