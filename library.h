#ifndef _LIBRARY_H
#define _LIBRARY_H

#include "date.h"

#define USER_DB             "users.db"
#define BOOK_DB             "book.db"
#define BOOKCOPY_DB	        "bookcopies.db"
#define ISSUERECORD_DB	    "issuerecord.db"


#define ROLE_OWNER          "owner"
#define ROLE_LIBRARIAN      "librarian"
#define ROLE_MEMBER          "member"

#define STATUS_AVAIL        "available"
#define STATUS_ISSUED        "issued"

#define PAY_TYPE_FEES        "fees"
#define PAY_TYPE_FINE        "fine"

#define FINE_PER_DAY              5
#define BOOK_RETURN_DAYS          7
#define MEMBERSHIP_MONTH_DAYS     30

#define EMAIL_OWNER		"sapkalgovind7@gmail.com"

typedef struct user 
{
    int id;
    char name[50];
    char email[50];
    char phone[12];
    char pwd[15];
    char role[20];
}user_t;

typedef struct book 
{
     int id;
     char name[50];
     char author[50];
     char subject[20];
     double price; 
     int isbn[20];
}book_t;

typedef struct bookcopy 
{
    int id;
    int bookid;
    int rack;
    char status[20];
}bookcopy_t;

typedef struct issuerecord 
{
    int id;
    int copyid;
    int memberid;
    date_t issue_date;
    date_t return_duedate;
    date_t return_date;
    double fine_amount;
}issuerecord_t;

typedef struct payment 
{
    int id;
    int memberid;
    double amount;
    char type[20];
    date_t tx_time;
    date_t next_pay_duedate;
}payment_t;

//  user functions declarations
 void user_accept(user_t *u);
 void user_display(user_t *u);

//bookcopies functions declarations
void bookcopy_accept(bookcopy_t *bc);
void bookcopy_display(bookcopy_t *bc);

// book functions declaration.
void book_accept(book_t *b);
void book_display(book_t *b);

// issuerecord functions declarations
void issuerecord_accept(issuerecord_t *ir);
void issuerecord_display(issuerecord_t *ir);

//payment functions declarations
void payment_accept(payment_t *p);
void payment_display(payment_t *p);

// librarian functions declaration
void librarian_area(user_t *u);
void add_new_member();
void add_book();
void book_edit_by_id();
void bookcopy_checkavailable_details();
void bookcopy_issue();
void bookcopy_changestatus(int bookcopy_id, char status[]);
void display_issued_bookcopies(int member_id);
void bookcopy_return();




// member functions declaration
void member_area(user_t *u);
void bookcopy_check_available();

// common functions declaration
void sign_in();
void sign_up();
int get_max_user_id();
int get_max_book_id();
void book_find_by_name(char name[]);
int get_max_issuerecord_id();
void edit_profile_by_id();
void change_pwd_by_email();
//void edit_profile(user_t *u);
//void change_password(user_t *u);

// add user function.
void add_user(user_t *u);


//find perticular user function.
int find_user_by_email(user_t *u,char email[]);


// owner functions declaration.

void owner_area(user_t *u);
void appoint_libarian();

#endif
